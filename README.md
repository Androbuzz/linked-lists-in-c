This project focuses on the implementation of linked lists in C, as part of the Data Structures and Algorithms course at Makerere University. 

The code includes functions for creating, printing, appending, prepending, deleting, and inserting nodes in a linked list. 

These implementations serve as practical examples to understand linked lists and their operations.
